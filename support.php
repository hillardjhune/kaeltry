<!DOCTYPE html>
<html>
<head>
	<title>Support Page</title>
	<link rel="stylesheet" type="text/css" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
</head>
<?php
    include('header.php');
?>
<style>
	body {
	font-family: Arial, sans-serif;
	margin: 0;
	padding: 0;
}

header {
	background-color: #ffffff;
	color: rgb(0, 0, 0);
	padding: 20px;
	text-align: center;
}

h1 {
	margin: 0;
}

main {
	max-width: 800px;
	margin: 0 auto;
	padding: 20px;
}

section {
	margin-bottom: 40px;
}

h2 {
	margin-top: 0;
}

form {
	display: flex;
	flex-direction: column;
}

label, input, textarea {
	margin-bottom: 10px;
}

input[type="submit"] {
	background-color: #4CAF50;
	color: white;
	padding: 10px 15px;
	border: none;
	border-radius: 3px;
	cursor: pointer;
}

input[type="submit"]:hover {
	background-color: #3e8e41;
}

dl {
	margin: 0;
}

dt {
	font-weight: bold;
	margin-top: 20px;
}

dd {
	margin-left: 0;
	margin-bottom: 20px;
}

.success-message {
	background-color: #d4edda;
	border: 1px solid #c3e6cb;
	color: #155724;
	padding: 10px;
	margin-bottom: 20px;
}

.error-message {
	background-color: #f8d7da;
	border: 1px solid #f5c6cb;
	color: #721c24;
	padding: 10px;
	margin-bottom: 20px;
}

</style>
<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

<?php
    include ('menu.php');
?>
	   <!-- Content Wrapper -->
	   <div id="content-wrapper" class="d-flex flex-column">

<!-- Main Content -->
<div id="content">

	<!-- Topbar -->
	 
	<nav class="navbar navbar-expand navbar-dark bg-white topbar mb-4 static-top shadow">
	   <!-- Sidebar Toggle (Topbar) -->
	   <button id="backbutton" class="btn btn-link rounded-circle mr-3" onclick="history.back()">
			<i class="fas fa-arrow-left"></i>
		</button>
			<h1 class="text-black">Support</h1>
	   

		<!-- Topbar Navbar -->
		<ul class="navbar-nav ml-auto">



			<!-- Nav Item - Search Dropdown (Visible Only XS) -->
			<li class="nav-item dropdown no-arrow d-sm-none">
				<a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
					data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-search fa-fw"></i>
				</a>
				<!-- Dropdown - Messages -->
				<div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
					aria-labelledby="searchDropdown">
					<form class="form-inline mr-auto w-100 navbar-search">
						<div class="input-group">
							<input type="text" class="form-control bg-light border-0 small"
								placeholder="Search for..." aria-label="Search"
								aria-describedby="basic-addon2">
							<div class="input-group-append">
								<button class="btn btn-primary" type="button">
									<i class="fas fa-search fa-sm"></i>
								</button>
							</div>
						</div>
					</form>
				</div>
			</li>

		</ul>

	</nav>
	<!-- End of Topbar -->

        <!-- Main table -->
	<main>
		<section>
			<h2 style= "text-align: center;">Contact Us</h2>
			<p>If you need help or have any questions, please don't hesitate to contact us.</p>
			<form id="contact-form">
				<label for="name">Name:</label>
				<input type="text" id="name" name="name" required>
				<label for="email">Email:</label>
				<input type="email" id="email" name="email" required>
				<label for="message">Message:</label>
				<textarea id="message" name="message" rows="5" required></textarea>
				<input type="submit" value="Submit">
			</form>
			<div id="response-message"></div>
		</section>
		<section>
			<h2>FAQs</h2>
			<dl>
				<dt>How do I reset my password?</dt>
				<dd>Visit the login page and click "Forgot Password". Follow the prompts to reset your password.</dd>
				<dt>How do I create new account?</dt>
				<dd>Visit the login page and click "Create Account". Follow the prompts to create new account.</dd>
			</dl>
		</section>
	</main>
	<footer style="text-align: center;">
		<span>Brgy. Guintoylan Project &copy; Khayle Dellezo 2023</span>
	</footer>

	<script src="script.js"></script>


     <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/js/bootstrap.bundle.min.js" integrity="sha384-qKXV1j0HvMUeCBQ+QVp7JcfGl760yU08IQ+GpUo5hlbpg51QRiuqHAJz8+BrxE/N" crossorigin="anonymous"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>
</body>
</html>

<script>
	const form = document.getElementById("contact-form");
const responseMessage = document.getElementById("response-message");

form.addEventListener("submit", function(event) {
	event.preventDefault();

	const formData = new FormData(form);
	const xhr = new XMLHttpRequest();
	xhr.open("POST", "submit-form.php");

	xhr.onreadystatechange = function() {
		if (xhr.readyState === 4) {
			if (xhr.status === 200) {
				responseMessage.classList.add("success-message");
				responseMessage.textContent = "Thank you for contacting us!";
				form.reset();
			} else {
				responseMessage.classList.add("error-message");
				responseMessage.textContent = "An error occurred. Please try again later.";
			}
		}
	};

	xhr.send(formData);
});

</script>
