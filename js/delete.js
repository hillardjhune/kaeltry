  // Class Delete scripts  //  
$(document).ready(function () {

    $('.class_delete_confirm_btn').click(function(e) {
       //  var form = document.forms["dataTable"];
         e.preventDefault();

         var id = $(this).val();
         swal({
           title: 'Are you sure?',
           text: "You won't be able to revert this!!",
           icon: 'warning',
           buttons: true,
           dangerMode: true,
         })
         .then((willDelete) => {
           if (willDelete) {
             $.ajax({
                  method: "POST",
                  url: "deleteinfo.php",
                  data: {
                      'id':id,
                      'class_delete_confirm_btn': true

                  },
                  success: function(response){
                     if(response == 200)
                     {
                        swal("Success!","Information Deleted!", "success");
                        $(".class_delete_confirm_btn").load(location.href = "OrderTable.php");
                     }
                     else if(response == 500)
                     {
                       swal("Oops!","Save Error!", "error");
                     }
                  }
               });
           }
         })
     });
 });
 