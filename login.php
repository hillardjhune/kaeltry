
<!DOCTYPE html>
<html lang="en">
    <head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
    </head>
    <style>

img {
    margin-left: 60px;
    margin-top: 60px;
    width: 280px;
    height: 270px;
    
}
.container {
    margin-top:40px;
    
}

    </style>

<?php
    include('header.php');
?>

<body class="bg-gray">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center bg">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5 bg-dark">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row bg-white">
                            <div class="col-lg-4 d-none d-lg-block"><img src="images/Guintoylan.jpg"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-black mb-4">Welcome Back Admin!</h1>
                                    </div>
                                    <div id = "msg"></div>
                                    <form class="user">
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-user"
                                                id="username" aria-describedby="emailHelp"
                                                placeholder="Enter Username">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user"
                                                id="password" placeholder="Password">
                                        </div>
                                        <a href="#" class="btn btn-primary btn-user btn-block" id="btnLogin">
                                            Login
                                        </a>
                                        <hr>
                                    <div class="text-center">
                                        <a class="btn btn-success btn-user btn-block" href="register.php">Create an Account!</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href="forgot-password.php">forgot password</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>

</html>

<script type="text/javascript">
    $("#btnLogin").click(function(){
        var Username = $("#username").val();
        var Password = $("#password").val();
        $("#btnLogin").text("Logging in...")
        if (Username == ""){
            swal({
              title: "Error",
              text: "Empty Username!",
              icon: "error",
              button: "OK!",
            });
            return false;
        }

        if (Password == ""){
            swal({
              title: "Error",
              text: "Empty password!",
              icon: "error",
              button: "OK!",
            });
            return false;
        }

        $.ajax({
            url:"processLogin.php",
            method: "post",
            data:{"username":Username, "password": Password},
            success: function(res){
                if (res == "1"){
                    window.location = "index.php";
                }else{
                    $("#msg").html(res);
                }
                
            }
        });
    });
</script>