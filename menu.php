<!DOCTYPE html>
<html lang="en">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
</head>
    <style>
        img {
			border-radius:  1000%;
			border: 0px solid black;
		}

    </style>
<body>
<!-- Sidebar -->
<hr class="sidebar-divider">
<div class="topbar-divider d-none d-sm-block"></div>
<ul class="navbar-nav bg-dark sidebar sidebar-dark accordion" id="accordionSidebar">
<br>
        <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center"href="index.php">
        <div class="sidebar-brand-logo">
            <img src="images/Guintoylan.jpg" width="100" height="100">
        </div>
    </a>

   
   <!-- Divider -->
   <br>
   <hr class="sidebar-divider d-none d-md-block bg-white">


    
    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="Index.php">
            <i class="fas fa-fw fa-home"></i>
            <span>Main Menu</span></a>
    </li>


    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block bg-white">

    <!-- Heading -->
    <div class="sidebar-heading">
        
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
     <li class="nav-item">
        <a class="nav-link collapsed" href="Dashboard.php">
            <i class="fas fa-fw fa-bars"></i>
            <span>Dashboard</span>
        </a>   
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block bg-white">
    
    <li class="nav-item">
        <a class="nav-link collapsed" href="OrderTable.php">
            <i class="fa fa-user"></i>
            <span>Resident Information</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block bg-white">
    
    <li class="nav-item">
        <a class="nav-link collapsed" href="setting.php">
            <i class="fa fa-cog fa-spin"></i>
            <span>System Settings</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block bg-white">
    
    <li class="nav-item">
        <a class="nav-link collapsed" href="support.php">
            <i class="fas fa-fw fa-user-circle"></i>
            <span>Support</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block bg-white">
    

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
            aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-info"></i>
            <span>About</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header" >Guintoylan</h6>
                <a class="collapse-item" href="History.php">History</a>
                <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header" ></h6>
            </div>
            </div>
        </div>
    </li>

<div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
    
</ul>
<!-- End of Sidebar -->
    <!-- Divider -->
    <hr class="sidebar-divider my-0 bg-white">


    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
    

</ul>
<!-- End of Sidebar -->
<body>