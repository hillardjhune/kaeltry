 <?php
 session_start();
include('connection.php');
 ?>


<!DOCTYPE html>
<html lang="en">

<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Brgy. Guintoylan - Register Accrount</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>
<style>

img {
    margin-left: 60px;
    margin-top: 60px;
    width: 280px;
    height: 270px;
    
}
.container {
    margin-top:40px;
    
}


</style>

<body class="bg-gray">

    <div class="container">

        <div class="row justify-content-center bg">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5 bg-dark">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row bg-white">
                                <div class="col-lg-4 d-none d-lg-block"><img src="images/Guintoylan.jpg" style="width:100%"></div>
                                <div class="col-lg-6">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="h4 text-black mb-4">Create an Account!</h1>
                                        </div>
                                            <form action="processRegister.php" method="post">
                                                <input type="hidden" name="hiddenID" value="<?=$id?>">
                                                <div class="form-group">
                                                    <input type="text" class="form-control form-control-user" name="UserName"
                                                        placeholder="User Name" required>
                                            </div>
                                            <div class="form-group">
                                                    <input type="password" class="form-control form-control-user"
                                                        name="Password" placeholder="Password" required>
                                            
                                            </div>
                                            <button class="btn btn-success btn-user btn-block" type="submit">Register Account</button>
                                            
                                            <hr>
                                        </form>
                                        <hr>
                                        <div class="text-center">
                                            <a class="small" href="login.php">Already have an account? Login!</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>

   <!-- Bootstrap core JavaScript-->
   <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>
</html>