
<!DOCTYPE html>
<html lang="en">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
</head>
<?php
    include('header.php');
?>
<style>

.support {
	background-color: #3e3e3e;
	border: none;
	color: white;
	padding: 12px 24px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 16px;
	margin: 4px 2px;
	cursor: pointer;
	border-radius: 3px;
}
.support {
	margin-right: 8px;
}

</style>


<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <?php
        include ('menu.php');
    ?>
    <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                 
                <nav class="navbar navbar-expand navbar-dark bg-white topbar mb-4 static-top shadow">
                   <!-- Sidebar Toggle (Topbar) -->
                   <button id="backbutton" class="btn btn-link rounded-circle mr-3" onclick="history.back()">
                        <i class="fas fa-arrow-left"></i>
                    </button>
                        <h1 class="text-black">System Setting</h1>
                   

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">



                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                                aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small"
                                            placeholder="Search for..." aria-label="Search"
                                            aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                
                <!-- Total Population -->
        <div class="col">

                        <div class="col-xl-3 col-md-6 mb-3">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-right">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                </div>
                                             
                                                    
                         
                                                    <div class="text-center  h5 mb-0 font-weight-bold text-white-800" >Add Information</div>
                                                  

                                                
                                            
                                        </div>
                                        <button class="btn btn-primary btn-lg mb-3 mt-6" onclick="location.href='addinfo.php'">Add information
                                        <i class="fas fa-fw fa-plus"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>


                         <!-- Male -->
                         <div class="col-xl-3 col-md-6 mb-3">
                            <div class="card border-left-success shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-right">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                </div>
                                             
                                                    
                         
                                                    <div class="text-center h5 mb-0 font-weight-bold text-white-800" >Support</div>
                                                  

                                                
                                            
                                        </div>
                                        <button class="btn btn-success   btn-lg mb-3 mt-6" onclick="location.href='support.php'">Support
                                        <i class="fas fa-fw fa-user-circle"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- View Information -->
                        <div class="col-xl-3 col-md-6 mb-3">
                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-right">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                </div>
                                             
                                                    
                         
                                                    <div class="text-center h5 mb-0 font-weight-bold text-white-800" >View Information</div>
                                                  

                                                
                                            
                                                </div>
                                                <button class="btn btn-info btn-lg mb-3 mt-6" onclick="location.href='OrderTable.php'">View Information
                                        <i class="fas fa-fw fa-eye"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Logout -->
                        <div class="col-xl-3 col-md-6 mb-3">
                            <div class="card border-left-danger shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-right">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                </div>
                                             
                                                    
                         
                                                    <div class="text-center h5 mb-0 font-weight-bold text-white-800" >Logout</div>
                                                  

                                                
                                            
                                                </div>
                                                <button class="btn btn-danger btn-lg mb-3 mt-6" onclick="location.href='logout.php'">Logout
                                        <i class="fas fa-sign-out-alt  text-black-400"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
             
        </div>  
                        
</div>


            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Brgy. Guintoylan Project &copy; Khayle Dellezo 2023</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>
    <script src="assetsDT/js/bootstrap.bundle.min.js"></script>
    <script src="assetsDT/js/jquery-3.6.0.min.js"></script>
    <script src="assetsDT/js/pdfmake.min.js"></script>
    <script src="assetsDT/js/vfs_fonts.js"></script>
    <script src="assetsDT/js/custom.js"></script>
    <script src="assetsDT/js/datatables.min.js"></script>
    <script src="js/demo/datatables-demo.js"></script>

    <script src="vendor/sweetalert/sweetalert.min.js"></script>
    <!-- <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap5.min.js"></script> -->

    <!-- Sweet Alert Delete scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script src="js/sweetalert2.all.min.js"></script>
    <script src="js/sweetalert.min.js"></script>
    <script src="js/delete.js"></script>
   
       
  

    <script>
        $(document).ready(function () {
        $('#example').DataTable();
    });
    </script>

     <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
  </a>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/js/bootstrap.bundle.min.js" integrity="sha384-qKXV1j0HvMUeCBQ+QVp7JcfGl760yU08IQ+GpUo5hlbpg51QRiuqHAJz8+BrxE/N" crossorigin="anonymous"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
   
</body>

</html>