<?php
    session_start();

    include('connection.php');

    if (isset($_SESSION['hasLog'])){
        $haslog = $_SESSION['hasLog'];
    }else{
        $haslog = 0;
    }

    if (empty($haslog)){
        header("location: login.php");
        exit;
    }

    
?>


<!DOCTYPE html>
<html lang="en">

<?php
    include('header.php');
?>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-dark bg-white topbar mb-4 static-top shadow">
                    <button id="backbutton" class="btn btn-link rounded-circle mr-3" onclick="history.back()">
                        <i class="fas fa-arrow-left"></i>
                    </button>

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    
                        <h2 style="color: black;">Edit Info</h2>
                        <hr class="sidebar-divider my-0 bg-black">
                        
                        <?php
                            $id = $_GET['id'];
                            $sql = "select * from information where id = ".$id;
                            $result = $conn->query($sql);
                            $row = $result->fetch_assoc();
                        ?>
                        <form action = "editinfosave.php" method="post">
                            <input type="hidden" name="hiddenID" value="<?=$id?>">
                       <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="color: black;">Name</label>
                                    <input type="text" name="name" class="form-control" placeholder="Name" value="<?=$row['Name']?>" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="color: black;">Age</label>
                                    <input type="text" name="age" class="form-control" placeholder="How Old?" value="<?=$row['Age']?>" required>
                                </div> 
                            </div>
                           
                                 <div class="col-md-6">
                                    <div class="form-group"></div>
                                <label class="form-label">Gender</label>
                                    <select name="gender" id="gender" class="form-control"  value="<?=$row['Gender']?>" required>
                                        <option selected>Choose...</option>
                                        <option value="Male" >Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                            </div>
                            <div class="col-md-6">
                                    <div class="form-group"></div>
                                <label class="form-label">Civil Status</label>
                                    <select name="civilstatus" id="civilstatus" class="form-control" value="<?=$row['CivilStatus']?>" required>
                                        <option selected>Choose...</option>
                                        <option value="Single" >Single</option>
                                        <option value="Married">Married</option>
                                        <option value="Divorced">Divorced</option>
                                        <option value="Separated">Separated</option>
                                    </select>
                            </div>
                            </div>
                            <br>

                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success">Save Changes</button>
                            </div>
                        </div>
                    </form>



                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Brgy. Guintoylan Project &copy; Khayle Dellezo 2022</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="Properties/jquery/jquery.min.js"></script>
    <script src="Properties/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="Properties/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    

</body>

</html>
<script type="text/javascript">
    

    function ageCount() {
        var now =new Date();                            //getting current date
        var currentY= now.getFullYear();                //extracting year from the date
        var currentM= now.getMonth() + 1;                   //extracting month from the date
        var currentD= now.getDay() + 8;
          
        var dobget =document.getElementById("BDate").value; //getting user input
        var dob= new Date(dobget);                             //formatting input as date
        var prevY= dob.getFullYear();                          //extracting year from input date
        var prevM= dob.getMonth() + 1;                             //extracting month from input date
        var prevD= dob.getDay() + 9;


        var ageY =currentY - prevY;
        var ageM =Math.abs(currentM- prevM);

        if(currentD < prevD || currentM < prevM) {
            var ageN = ageY - 1;
            document.getElementById('age').value = ageN;  
        }
        else {
            var ageN = ageY;
            document.getElementById('age').value = ageN;  
        }
        
        
    }


</script>