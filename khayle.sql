-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2023 at 12:39 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `khayle`
--

-- --------------------------------------------------------

--
-- Table structure for table `information`
--

CREATE TABLE `information` (
  `id` int(255) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Age` int(255) NOT NULL,
  `Gender` varchar(255) NOT NULL,
  `CivilStatus` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `information`
--

INSERT INTO `information` (`id`, `Name`, `Age`, `Gender`, `CivilStatus`) VALUES
(4, 'Jocel Albino', 19, 'Female', 'Single'),
(5, 'Gerald Albino', 27, 'Male', 'Single'),
(6, 'Angelee Placido', 20, 'Female', 'Single'),
(7, 'Jay Almer Amolong', 11, 'Male', 'Single'),
(8, 'Retchel Asuncion', 36, 'Female', 'Single'),
(9, 'Cherryl Ann', 44, 'Female', 'Married'),
(10, 'Veronica Cabitana', 71, 'Female', 'Divorced'),
(11, 'Justine Asilo', 15, 'Male', 'Single'),
(12, 'Keneth Joshua Laruya', 24, 'Male', 'Single'),
(13, 'Jane Ramada', 63, 'Female', 'Married'),
(14, 'Nenita Cuares', 47, 'Female', 'Married'),
(15, 'Celso Tasong', 63, 'Male', 'Married'),
(16, 'John Ray Cuares', 11, 'Male', 'Single'),
(17, 'Baby James', 13, 'Male', 'Single'),
(18, 'Christopher Llemit', 20, 'Male', 'Single'),
(19, 'Fruto Jr. Cabitana', 74, 'Male', 'Married'),
(20, 'Renn Siolana', 9, 'Female', 'Single'),
(23, 'Khayle Dellezo', 21, 'Male', 'Single');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(255) NOT NULL,
  `Username` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `Username`, `Password`) VALUES
(1, 'khayle', 'Dellezo');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `information`
--
ALTER TABLE `information`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
