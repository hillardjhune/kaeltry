<?php
    session_start();
    include('connection.php');

    if (isset($_SESSION['hasLog'])){
        $haslog = $_SESSION['hasLog'];
    }else{
        $haslog = 0;
    }

    if (empty($haslog)){
        header("location: login.php");
        exit;
    }

    $sql = "select * from information ORDER BY name DESC";
    $results = $conn->query($sql);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
</head>

<?php
    include('header.php');
?>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">
<?php
    include('menu.php');
?>
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-wihte topbar mb-4 static-top shadow">
                         <!-- Sidebar Toggle (Topbar) -->
                    <button id="backbutton" class="btn btn-link rounded-circle mr-3" onclick="history.back()">
                        <i class="fas fa-arrow-left"></i>
                    </button>
                    <h1 class="text-black">History</h1>
                   

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">
                   

                </nav>
                <!-- End of Topbar -->
                <h1 class="text-center">Guintoylan</h1>
                <div class="container-fluid">



                    <section id='sectionLocation' aria-labelledby='sectionLocation-head'>
                        <h2 id='sectionLocation-head'>Location</h2>
                        <p class='articleContent'>
                            Guintoylan is situated at approximately 10.1219, 125.1586, in the island of <a href='physical/islands/panaon.html'>Panaon</a>
                            . Elevation at these coordinates is estimated at 263.4 meters or 864.2 feet above mean sea level.
                        </p>
                        <div id='mapWrapBrgy'>
                            <div id='map'></div>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15710.380609043857!2d125.15501917398008!3d10.13218638416878!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x330726dbbef6dc49%3A0xaf70bfd7daaef150!2sGuintoylan%2C%20Liloan%2C%20Southern%20Leyte!5e0!3m2!1sen!2sph!4v1680252001090!5m2!1sen!2sph" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                        </div>
                    </section>
                <br>
                <div>
                    <section id='sectionAdjBgys' aria-labelledby='sectionAdjBgys-head'>
                        <h2 id='sectionAdjBgys-head'>Adjacent barangays</h2>
                        <p class='articleContent'>Guintoylan shares a common border with the following barangay(s):</p>
                        <ul class='simList2col' id='adjacent-bgy-list'>
                            <li>
                                <a href='visayas/r08/southern-leyte/liloan/san-isidro.html'>San Isidro, Liloan, Southern Leyte</a>
                            <li>
                                <a href='visayas/r08/southern-leyte/liloan/cagbungalon.html'>Cagbungalon, Liloan, Southern Leyte</a>
                            <li>
                                <a href='visayas/r08/southern-leyte/liloan/candayuman.html'>Candayuman, Liloan, Southern Leyte</a>
                            <li>
                                <a href='visayas/r08/southern-leyte/liloan/estela.html'>Estela, Liloan, Southern Leyte</a>
                        </ul>

                <!-- Begin Page Content -->
               

                         <h2>Historical population</h2>
                    
                              <p>The population of Guintoylan grew from 707 in 1990 to 1,113 in 2020, an increase of 406 people over the course of 30 years. The latest census figures in 2020 denote a positive growth rate of 1.44%, or an increase of 73 people, from the previous population of 1,040 in 2015</p>

                         <h2>Households</h2>
                    
                             <p>The household population of Guintoylan in the 2015 Census was 1,040 broken down into 222 households or an average of 4.68 members per household.</p>
                </div>
                
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Guintoylan Project &copy; Khayle Dellezo 2023</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->
            <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/js/bootstrap.bundle.min.js" integrity="sha384-qKXV1j0HvMUeCBQ+QVp7JcfGl760yU08IQ+GpUo5hlbpg51QRiuqHAJz8+BrxE/N" crossorigin="anonymous"></script>
