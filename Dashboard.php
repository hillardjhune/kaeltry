<?php
    session_start();
    include('connection.php');

    if (isset($_SESSION['hasLog'])){
        $haslog = $_SESSION['hasLog'];
    }else{
        $haslog = 0;
    }

    if (empty($haslog)){
        header("location: login.php");
        exit;
    }

    $sql = "select * from information ORDER BY name DESC";
    $results = $conn->query($sql);
?>


<!DOCTYPE html>
<html lang="en">
    <head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
    </head>
    <style> 

.container {
  display: flex;
  flex-wrap: nowrap; /* Prevents wrapping to new line */
  overflow-x: auto; /* Allows horizontal scrolling */
}

.card {
  width: 300px; /* Set the width of the card */
  margin-right: 20px; /* Add some space between the card and table */
}

h3{
    background: #6495ED;
    color: white;
}
#example {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#example td, #example th {
  border: 1px solid #ddd;
  padding: 8px;
}

#example tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}


</style>

<?php
    include('header.php');
?>
<body id="page-top" class="containe-fluid">

    <!-- Page Wrapper -->
    <div id="wrapper">
<?php
    include('menu.php');
?>
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="backbutton" class="btn btn-link rounded-circle mr-3" onclick="history.back()">
                        <i class="fas fa-arrow-left"></i>
                    </button>
                        <h1 class="text-black">Dashboard</h1>
                        
                   
                    
                        
                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">
                        

                      
                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-black-600 small">Admin</span>
                                <img class="img-profile rounded-circle"
                                    src="img/undraw_profile.svg">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="logout.php">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-black-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>
                    </ul>

                </nav>

                <?php 
                    include('connection.php');
                ?>
              
        <div class="col-lg-8">
       
            
              <div class="panel-heading" style="background-color: white; border-color: gray; margin-bottom: 25px">
                <div class="row">
                  
                  <div class="col-lg-2">
                  <img src="images/Guintoylan.jpg" width="150" height="150">
                  </div>
                  
                  <div class="col-lg-6" style="font-size: 22px;margin-top: 5px; margin-left: 15px; font-family: Tahoma">
                   <center><h1>Brgy. Guintoylan </h1></center>
                   <center><i>Liloan Southern Leyte  </i></div></center>
                   
                </div>

              </div>
              
            </div> 
      </a> 
   

    <!-- Table -->
    <div class="row">
          
          

      
        <div class="row">
          <div class="col-lg-8">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title text-white text-center"><i class="fa fa-user"></i> Current Barangay Officials</h3>
              </div>
              <div class="panel-body">
                <div id="morris-chart-donut"></div>
                 <table class="table table-black table-striped table-hover table table-md" id="example">
                <thead>
                    <tr>
                    <th>Full Name</th>
                    <th>Chairmanship</th>
                    <th>Position</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <td>Nancy D. Monghit</td>
                    <td>Marketing Committee</td>
                    <td>Chairman</td>
                    </tr>
                    <tr>
                    <td>Tonet</td>
                    <td>Finance Committee</td>
                    <td>Treasurer</td>
                    </tr>
                    <tr>
                    <td>Laarne D. Crisostomo</td>
                    <td>Operations Committee</td>
                    <td>Secretary</td>
                    </tr>
                    <tr>
                    <td>Emilina A. CAstrudes</td>
                    <td>Committee on Appropriation</td>
                    <td>Brgy. Councilor 1</td>
                    </tr>
                    <tr>
                    <td>Ebeth C. Ocado</td>
                    <td>Committee on Peace & Order</td>
                    <td>Brgy. Councilor 2</td>
                    </tr>
                    <tr>
                    <td>Edil M. Anajao</td>
                    <td>Committee on Health</td>
                    <td>Brgy. Councilor 3</td>
                    </tr>
                    <tr>
                    <td>Barcilisa P. Remoticado</td>
                    <td>Committee on Education</td>
                    <td>Brgy. Councilor 4</td>
                    </tr>
                    <tr>
                    <td>Roberto Castrudes</td>
                    <td>Committee on Rules</td>
                    <td>Brgy. Councilor 5</td>
                    </tr>
                    <tr>
                    <td>Stephane Ty</td>
                    <td>Committee on Infra</td>
                    <td>Brgy. Councilor 6</td>
                    </tr>
                    <tr>
                    <td>Maica Tasong</td>
                    <td>Committee on Sports</td>
                    <td>Brgy. SK Chairman</td>
                    </tr>
                </tbody>
                </table>
            </div>
          </div>
        </div>
        

           <!-- Total Population -->
        <div class="col">
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-right">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Total Population:</div>
                                             <?php
                                                $sql = "SELECT count(id) as Name FROM information";  
                                                $results = $conn->query($sql);

                                                foreach($results as $row){
                                                    
                                                    echo '<div class="text-center h5 mb-0 font-weight-bold text-white-800" >'.$row['Name'].'</div>';
                                                    

                                                }
                                            ?>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-user fa-2x text-black-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                         <!-- Male -->
                        <div class="col-xl-3 col-md-6 mb-4">

                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                MALE</div>
                                            <div class="h5 mb-0 font-weight-bold text-black-800">Population</div>
                                            <?php
                                                $sql = "SELECT count(id) as Gender FROM information Where Gender = 'Male'";  
                                                $results = $conn->query($sql);

                                                foreach($results as $row){
                                                    
                                                    echo '<div class="text-center h5 mb-0 font-weight-bold text-white-800" >'.$row['Gender'].'</div>';
                                                    

                                                }
                                            ?>
                                        </div>
                                        <div class="col-auto">
                                        <i class="fa fa-male fa-4x"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Female -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-warning shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                FEMALE</div>
                                            <div class="h5 mb-0 font-weight-bold text-black-800">Population</div>
                                            <?php
                                                $sql = "SELECT count(id) as Gender FROM information Where Gender = 'Female'";  
                                                $results = $conn->query($sql);

                                                foreach($results as $row){
                                                    
                                                    echo '<div class="text-center h5 mb-0 font-weight-bold text-white-800" >'.$row['Gender'].'</div>';
                                                    

                                                }
                                            ?>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-female fa-4x"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>     
             
        </div>  
                        
    </div>      
                

                               
            <!-- Footer -->
            <div class="">
                    <div class="copyright text-center my-auto text-black">
                        <span>Brgy. Guintoylan Project &copy; Khayle Dellezo 2023</span>
                    </div>
            </div>
 
          
            <!-- End of Footer -->
            <!-- End of Content Wrapper -->

    <!-- End of Page Wrapper -->
    
   

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/js/bootstrap.bundle.min.js" integrity="sha384-qKXV1j0HvMUeCBQ+QVp7JcfGl760yU08IQ+GpUo5hlbpg51QRiuqHAJz8+BrxE/N" crossorigin="anonymous"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    


</body>

</html>

